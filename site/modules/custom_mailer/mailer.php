<?php

/**
 * Custom Caracal Mailer
 *
 * Author: Mladen Mijatov
 */
namespace Modules\CustomMailer;

use \ContactForm_Mailer;


class Mailer extends ContactForm_Mailer {
	private $variables = array();

	/**
	 * Get localized name.
	 *
	 * @return string
	 */
	public function get_title() {
		return 'CTM - Call data updater';
	}

	public function start_message() {}

	/**
	 * Finalize message and send it to specified addresses.
	 *
	 * Note: Before sending, you *must* check if contact_form
	 * function detectBots returns false.
	 *
	 * @return boolean
	 */
	public function send() {
		global $ctm_access_key, $ctm_secret_key;

		$account_id = '322315';
		$form_id = 'FRT472ABB2C5B9B141ABD39BA16DF6F40143A610E0FB222DCE7893CB57F6316E3DC';
		$ctm_token = base64_encode($ctm_access_key.':'.$ctm_secret_key);
		$result = false;

		// find a call with specified phone number
		$url = "https://api.calltrackingmetrics.com/api/v1/accounts/${account_id}/calls/search.json";
		$matches = array();
		preg_match_all(';\d{1,};', $this->variables['phone_number'], $matches);
		$phone_number = implode('', $matches[0]);
		$phone_number = preg_replace(';0{0,}(\d+);', '\1', $phone_number);
		$parameters = array(
			'account_id' => $account_id,
			'filter'     => "contact_number:'${phone_number}'"
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Authorization: Basic ${ctm_token}",
			'Content-Type: application/json'
		));
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($parameters));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$raw_response = curl_exec($ch);
		curl_close($ch);

		$call_id = null;
		$call_exists = false;

		try {
			$response = json_decode($raw_response);
			if (property_exists($response, 'calls') && count($response->calls) > 0) {
				$call_id = array_pop($response->calls)->id;
				$call_exists = true;
			}
		} catch (Exception $error) {
			error_log($error->getMessage());
		}

		// update call data
		$parameters = array(
			'account_id'    => $account_id,
		);

		if ($call_exists) {
			// update existing call data
			$url = "https://api.calltrackingmetrics.com/api/v1/accounts/${account_id}/calls/${call_id}/modify.json";
			$parameters['call_id'] = $call_id;
			$parameters['custom_fields'] = array(
				'clinic'   => $this->variables['clinic'],
				'isclient' => $this->variables['is_already_client'] ? 1 : 0,
				'message'    => $this->variables['client_note']
			);
			$parameters['name'] = $this->variables['first_name'].' '.$this->variables['last_name'];

		} else {
			// submit form data since call doesn't exist
			$url = "https://api.calltrackingmetrics.com/api/v1/formreactor/${form_id}";
			$parameters['unique_form_id'] = $form_id;
			$parameters['phone_number'] = $this->variables['phone_number'];
			$parameters['caller_name'] = $this->variables['first_name'].' '.$this->variables['last_name'];
			$parameters['country_code'] = '972';
			$parameters['custom_clinic'] = $this->variables['clinic'];
			$parameters['custom_isclient'] = $this->variables['is_already_client'] ? 1 : 0;
			$parameters['custom_message'] = $this->variables['client_note'];
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Authorization: Basic ${ctm_token}",
			'Content-Type: application/json'
		));
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($parameters));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$raw_response = curl_exec($ch);
		curl_close($ch);

		try {
			$response = json_decode($raw_response);
			$result = property_exists($response, 'status') && $response->status == 'success';
		} catch (Exception $error) {
			error_log($error->getMessage());
		}

		return $result;
	}

	/**
	 * Set variables to be replaced in subject and body.
	 *
	 * @param array $params
	 */
	public function set_variables($variables) {
		$this->variables = $variables;
	}

	public function set_sender($address, $name=null) {}
	public function add_recipient($address, $name=null) {}
	public function add_cc_recipient($address, $name=null) {}
	public function add_bcc_recipient($address, $name=null) {}
	public function add_header_string($key, $value) {}
	public function set_subject($subject) {}
	public function set_body($plain_body, $html_body=null) {}
	public function attach_file($file_name, $attached_name=null, $inline=false) {}
}

?>
