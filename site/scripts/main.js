/**
 * Main JavaScript
 * Site Name
 *
 * Copyright (c) 2018. by Way2CU, http://way2cu.com
 * Authors:
 */

// create or use existing site scope
var Site = Site || {};

// make sure variable cache exists
Site.variable_cache = Site.variable_cache || {};


/**
 * Check if site is being displayed on mobile.
 * @return boolean
 */
Site.is_mobile = function() {
	var result = false;

	// check for cached value
	if ('mobile_version' in Site.variable_cache) {
		result = Site.variable_cache['mobile_version'];

	} else {
		// detect if site is mobile
		var elements = document.getElementsByName('viewport');

		// check all tags and find `meta`
		for (var i=0, count=elements.length; i<count; i++) {
			var tag = elements[i];

			if (tag.tagName == 'META') {
				result = true;
				break;
			}
		}

		// cache value so next time we are faster
		Site.variable_cache['mobile_version'] = result;
	}

	return result;
};

/**
 * Function called when document and images have been completely loaded.
 */
Site.on_load = function() {
	if (Site.is_mobile())
		Site.mobile_menu = new Caracal.MobileMenu();

	// fade disabled note behavior for already a client checkbox
	var is_client_label = document.querySelector('form label.checkbox');
	var client_note_label = is_client_label.nextSibling;

	client_note_label.classList.add('disabled');
	is_client_label.querySelector('input').addEventListener('change', function(event) {
		var input = event.currentTarget;
		if (input.checked)
			client_note_label.classList.remove('disabled'); else
			client_note_label.classList.add('disabled');
	});
};


// connect document `load` event with handler function
window.addEventListener('load', Site.on_load);
